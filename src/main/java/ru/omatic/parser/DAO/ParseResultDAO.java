package ru.omatic.parser.DAO;

import ru.omatic.parser.dbase.ParseResult;

import java.sql.SQLException;
import java.util.List;

public interface ParseResultDAO {
    public void addResult(ParseResult result) throws SQLException;

    public void updateResult(ParseResult result) throws SQLException;

    public ParseResult getResultById(Long id) throws SQLException;

    public List getLastResultByTaskId(Integer id) throws SQLException;

    public List getAllResults() throws SQLException;

    public void deleteResult(ParseResult result) throws SQLException;
}