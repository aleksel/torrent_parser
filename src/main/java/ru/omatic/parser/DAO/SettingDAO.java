package ru.omatic.parser.DAO;

import ru.omatic.parser.dbase.Setting;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

public interface SettingDAO {
    public void addSetting(Setting setting) throws SQLException;

    public void updateSetting(Setting setting) throws SQLException;

    public Setting getSettingById(Long id) throws SQLException;

    public List getAllSettings() throws SQLException;

    public Map getAllSettingsAsMap() throws SQLException;

    public void deleteSetting(Setting setting) throws SQLException;
}