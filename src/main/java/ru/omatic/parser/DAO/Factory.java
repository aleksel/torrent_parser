package ru.omatic.parser.DAO;

import ru.omatic.parser.DAO.Impl.CategoryDAOImpl;
import ru.omatic.parser.DAO.Impl.ParseResultDAOImpl;
import ru.omatic.parser.DAO.Impl.SettingDAOImpl;
import ru.omatic.parser.DAO.Impl.TaskDAOImpl;

public class Factory {

    private static TaskDAOImpl taskDAO = null;
    private static CategoryDAOImpl categoryDAO = null;
    private static ParseResultDAOImpl parseResultDAO = null;
    private static SettingDAOImpl settingDAO = null;
    private static Factory instance = null;

    public static synchronized Factory getInstance(){
        if (instance == null){
            instance = new Factory();
        }
        return instance;
    }

    public TaskDAOImpl getTaskDAO(){
        if (taskDAO == null){
            taskDAO = new TaskDAOImpl();
        }
        return taskDAO;
    }

    public CategoryDAOImpl getCategoryDAO(){
        if (categoryDAO == null){
            categoryDAO = new CategoryDAOImpl();
        }
        return categoryDAO;
    }

    public ParseResultDAOImpl getParseResultDAO(){
        if (parseResultDAO == null){
            parseResultDAO = new ParseResultDAOImpl();
        }
        return parseResultDAO;
    }

    public SettingDAOImpl getSettingDAO(){
        if (settingDAO == null){
            settingDAO = new SettingDAOImpl();
        }
        return settingDAO;
    }
}
