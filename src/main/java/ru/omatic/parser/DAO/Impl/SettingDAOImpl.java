package ru.omatic.parser.DAO.Impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.Logger;
import org.hibernate.Session;
import org.hibernate.Transaction;
import ru.omatic.parser.DAO.SettingDAO;
import ru.omatic.parser.dbase.Setting;
import ru.omatic.parser.utils.HibernateUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class SettingDAOImpl implements SettingDAO {
    private final Logger logger = (Logger) LogManager.getLogger(SettingDAOImpl.class);
    @Override
    public void addSetting(Setting setting) {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().getCurrentSession();
            session.beginTransaction();
            session.save(setting);
            session.getTransaction().commit();
        } catch (Exception e) {
            String cause = e.getCause() == null ? "" : ", cause: " + e.getCause().getMessage();
            logger.error("Ошибка I/O: " + e.getMessage() + cause);
        } finally {
        }
    }

    @Override
    public void updateSetting(Setting setting) {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().getCurrentSession();
            session.beginTransaction();
            session.update(setting);
            session.getTransaction().commit();
        } catch (Exception e) {
            String cause = e.getCause() == null ? "" : ", cause: " + e.getCause().getMessage();
            logger.error("Ошибка I/O: " + e.getMessage() + cause);
        } finally {
        }
    }

    @Override
    public Setting getSettingById(Long id) {
        Session session = null;
        Setting setting = null;
        try {
            session = HibernateUtil.getSessionFactory().getCurrentSession();
            setting = (Setting) session.load(Setting.class, id);
        } catch (Exception e) {
            String cause = e.getCause() == null ? "" : ", cause: " + e.getCause().getMessage();
            logger.error("Ошибка I/O: " + e.getMessage() + cause);
        } finally {
        }
        return setting;
    }

    @Override
    public List getAllSettings() {
        Session session = null;
        List<Setting> settings = new ArrayList<Setting>();
        try {
            session = HibernateUtil.getSessionFactory().getCurrentSession();
            Transaction transaction = session.beginTransaction();
            settings = session.createCriteria(Setting.class).list();
            transaction.commit();
        } catch (Exception e) {
            String cause = e.getCause() == null ? "" : ", cause: " + e.getCause().getMessage();
            logger.error("Ошибка I/O: " + e.getMessage() + cause);
        } finally {
        }
        return settings;
    }

    @Override
    public HashMap getAllSettingsAsMap() {
        List<Setting> allSettings = this.getAllSettings();

        HashMap<String, String> settings = new HashMap<String, String>();
        for (Setting setting : allSettings) {
            settings.put(setting.getSettingSystemName(), setting.getSettingValue());
        }

        return settings;
    }

    @Override
    public void deleteSetting(Setting setting) {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().getCurrentSession();
            session.beginTransaction();
            session.delete(setting);
            session.getTransaction().commit();
        } catch (Exception e) {
            String cause = e.getCause() == null ? "" : ", cause: " + e.getCause().getMessage();
            logger.error("Ошибка I/O: " + e.getMessage() + cause);
        } finally {
        }
    }
}