package ru.omatic.parser.DAO.Impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.Logger;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import ru.omatic.parser.DAO.CategoryDAO;
import ru.omatic.parser.dbase.TaskCategory;
import ru.omatic.parser.utils.HibernateUtil;

import java.util.ArrayList;
import java.util.List;

public class CategoryDAOImpl implements CategoryDAO {
    private final Logger logger = (Logger) LogManager.getLogger(CategoryDAOImpl.class);
    @Override
    public void addCategory(TaskCategory category) {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().getCurrentSession();
            session.beginTransaction();
            session.save(category);
            session.getTransaction().commit();
        } catch (Exception e) {
            String cause = e.getCause() == null ? "" : ", cause: " + e.getCause().getMessage();
            logger.error("Ошибка I/O: " + e.getMessage() + cause);
        } finally {
        }
    }

    @Override
    public void updateCategory(TaskCategory category) {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().getCurrentSession();
            session.beginTransaction();
            session.update(category);
            session.getTransaction().commit();
        } catch (Exception e) {
            String cause = e.getCause() == null ? "" : ", cause: " + e.getCause().getMessage();
            logger.error("Ошибка I/O: " + e.getMessage() + cause);
        } finally {
        }
    }

    @Override
    public TaskCategory getCategoryById(Long id) {
        Session session = null;
        TaskCategory category = null;
        try {
            session = HibernateUtil.getSessionFactory().getCurrentSession();
            category = (TaskCategory) session.load(TaskCategory.class, id);
        } catch (Exception e) {
            String cause = e.getCause() == null ? "" : ", cause: " + e.getCause().getMessage();
            logger.error("Ошибка I/O: " + e.getMessage() + cause);
        } finally {
        }
        return category;
    }

    @Override
    public List getAllCategories() {
        Session session = null;
        List<TaskCategory> categories = new ArrayList<TaskCategory>();
        try {
            session = HibernateUtil.getSessionFactory().getCurrentSession();
            categories = session.createCriteria(TaskCategory.class).list();
        } catch (Exception e) {
            String cause = e.getCause() == null ? "" : ", cause: " + e.getCause().getMessage();
            logger.error("Ошибка I/O: " + e.getMessage() + cause);
        } finally {
        }
        return categories;
    }

    @Override
    public List getAllActiveCategories() {
        Session session = null;
        List<TaskCategory> categories = new ArrayList<TaskCategory>();
        try {
            session = HibernateUtil.getSessionFactory().getCurrentSession();
            categories = session
                    .createCriteria(TaskCategory.class)
                    .add(Restrictions.eq("taskCategoryActive", new Integer(1)))
                    .list();
        } catch (Exception e) {
            String cause = e.getCause() == null ? "" : ", cause: " + e.getCause().getMessage();
            logger.error("Ошибка I/O: " + e.getMessage() + cause);
        } finally {
        }
        return categories;
    }

    @Override
    public void deleteCategory(TaskCategory category) {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().getCurrentSession();
            session.beginTransaction();
            session.delete(category);
            session.getTransaction().commit();
        } catch (Exception e) {
            String cause = e.getCause() == null ? "" : ", cause: " + e.getCause().getMessage();
            logger.error("Ошибка I/O: " + e.getMessage() + cause);
        } finally {
        }
    }
}
