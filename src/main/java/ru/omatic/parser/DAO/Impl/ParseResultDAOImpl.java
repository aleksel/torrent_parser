package ru.omatic.parser.DAO.Impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.Logger;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import ru.omatic.parser.DAO.ParseResultDAO;
import ru.omatic.parser.dbase.ParseResult;
import ru.omatic.parser.utils.HibernateUtil;

import java.util.ArrayList;
import java.util.List;

public class ParseResultDAOImpl implements ParseResultDAO {
    private final Logger logger = (Logger) LogManager.getLogger(ParseResultDAOImpl.class);
    @Override
    public void addResult(ParseResult result) {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().getCurrentSession();
            session.beginTransaction();
            session.save(result);
            session.getTransaction().commit();
        } catch (Exception e) {
            String cause = e.getCause() == null ? "" : ", cause: " + e.getCause().getMessage();
            logger.error("Ошибка I/O: " + e.getMessage() + cause);
        } finally {
        }
    }

    @Override
    public void updateResult(ParseResult result) {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().getCurrentSession();
            session.beginTransaction();
            session.update(result);
            session.getTransaction().commit();
        } catch (Exception e) {
            String cause = e.getCause() == null ? "" : ", cause: " + e.getCause().getMessage();
            logger.error("Ошибка I/O: " + e.getMessage() + cause);
        } finally {
        }
    }

    @Override
    public ParseResult getResultById(Long id) {
        Session session = null;
        ParseResult result = null;
        try {
            session = HibernateUtil.getSessionFactory().getCurrentSession();
            result = (ParseResult) session.load(ParseResult.class, id);
        } catch (Exception e) {
            String cause = e.getCause() == null ? "" : ", cause: " + e.getCause().getMessage();
            logger.error("Ошибка I/O: " + e.getMessage() + cause);
        } finally {
        }
        return result;
    }

    @Override
    public List getLastResultByTaskId(Integer id) {
        Session session = null;
        List<ParseResult> result = new ArrayList<ParseResult>();
        try {
            session = HibernateUtil.getSessionFactory().getCurrentSession();
            Transaction transaction = session.beginTransaction();
            result = session
                    .createCriteria(ParseResult.class)
                    .add(Restrictions.eq("parseResultTaskId", id))
                    .addOrder(Order.desc("parseResultId"))
                    .setMaxResults(1)
                    .list();
            transaction.commit();
        } catch (Exception e) {
            String cause = e.getCause() == null ? "" : ", cause: " + e.getCause().getMessage();
            logger.error("Ошибка I/O: " + e.getMessage() + cause);
        } finally {
        }
        return result;
    }

    @Override
    public List getAllResults() {
        Session session = null;
        List<ParseResult> results = new ArrayList<ParseResult>();
        try {
            session = HibernateUtil.getSessionFactory().getCurrentSession();
            results = session.createCriteria(ParseResult.class).list();
        } catch (Exception e) {
            String cause = e.getCause() == null ? "" : ", cause: " + e.getCause().getMessage();
            logger.error("Ошибка I/O: " + e.getMessage() + cause);
        } finally {
        }
        return results;
    }

    @Override
    public void deleteResult(ParseResult result) {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().getCurrentSession();
            session.beginTransaction();
            session.delete(result);
            session.getTransaction().commit();
        } catch (Exception e) {
            String cause = e.getCause() == null ? "" : ", cause: " + e.getCause().getMessage();
            logger.error("Ошибка I/O: " + e.getMessage() + cause);
        } finally {
        }
    }
}