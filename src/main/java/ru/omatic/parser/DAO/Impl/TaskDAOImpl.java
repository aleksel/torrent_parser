package ru.omatic.parser.DAO.Impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.Logger;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Expression;
import ru.omatic.parser.DAO.TaskDAO;
import ru.omatic.parser.dbase.Task;
import ru.omatic.parser.utils.HibernateUtil;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

public class TaskDAOImpl implements TaskDAO {
    private final Logger logger = (Logger) LogManager.getLogger(TaskDAOImpl.class);
    public void addTask(Task task) throws SQLException {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().getCurrentSession();
            session.beginTransaction();
            session.save(task);
            session.getTransaction().commit();
        } catch (Exception e) {
            String cause = e.getCause() == null ? "" : ", cause: " + e.getCause().getMessage();
            logger.error("Ошибка I/O: " + e.getMessage() + cause);
        } finally {
        }
    }

    public void updateTask(Task task) {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().getCurrentSession();
            session.beginTransaction();
            session.update(task);
            session.getTransaction().commit();
        } catch (Exception e) {
            String cause = e.getCause() == null ? "" : ", cause: " + e.getCause().getMessage();
            logger.error("Ошибка I/O: " + e.getMessage() + cause);
        } finally {
        }
    }

    public Task getTaskById(Long id) {
        Session session = null;
        Task task = null;
        try {
            session = HibernateUtil.getSessionFactory().getCurrentSession();
            task = (Task) session.load(Task.class, id);
        } catch (Exception e) {
            String cause = e.getCause() == null ? "" : ", cause: " + e.getCause().getMessage();
            logger.error("Ошибка I/O: " + e.getMessage() + cause);
        } finally {
        }
        return task;
    }

    public List<Task> getAllTasks() {
        Session session = null;
        List<Task> tasks = new ArrayList<Task>();
        try {
            session = HibernateUtil.getSessionFactory().getCurrentSession();
            tasks = session.createCriteria(Task.class).list();
        } catch (Exception e) {
            String cause = e.getCause() == null ? "" : ", cause: " + e.getCause().getMessage();
            logger.error("Ошибка I/O: " + e.getMessage() + cause);
        } finally {
        }
        return tasks;
    }

    public List<Task> getAllActiveTasks() {
        Session session = null;
        List<Task> tasks = new ArrayList<Task>();
        try {
            session = HibernateUtil.getSessionFactory().getCurrentSession();
            tasks = session.createCriteria(Task.class).add(Expression.eq("taskActive", new Integer(1))).list();
        } catch (Exception e) {
            String cause = e.getCause() == null ? "" : ", cause: " + e.getCause().getMessage();
            logger.error("Ошибка I/O: " + e.getMessage() + cause);
        } finally {
        }
        return tasks;
    }

    public void deleteTask(Task task) {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().getCurrentSession();
            session.beginTransaction();
            session.delete(task);
            session.getTransaction().commit();
        } catch (Exception e) {
            String cause = e.getCause() == null ? "" : ", cause: " + e.getCause().getMessage();
            logger.error("Ошибка I/O: " + e.getMessage() + cause);
        } finally {
        }
    }

    @Override
    public Queue getAllActiveTaskInActiveCategory() {
        List<Task> tasks = new ArrayList<Task>();
        Queue<Task> queue = new ConcurrentLinkedQueue<Task>();

        try {
            Session session = HibernateUtil.getSessionFactory().getCurrentSession();
            Transaction transaction = session.beginTransaction();
            SQLQuery query = session.createSQLQuery(
                    "SELECT * " +
                    "FROM task " +
                    "INNER JOIN task_has_task_category ON task_id = task_has_task_category_task_id " +
                    "INNER JOIN task_category ON task_has_task_category_task_category_id = task_category_id " +
                    "WHERE task_active = :active " +
                    "AND task_category_active = :active"
            ).addEntity(Task.class);
            tasks = query.setInteger("active", 1).list();
            transaction.commit();
            queue.addAll(tasks);
        } catch (Exception e) {
            String cause = e.getCause() == null ? "" : ", cause: " + e.getCause().getMessage();
            logger.error("Ошибка I/O: " + e.getMessage() + cause);
        } finally {
        }
        return queue;

    }
}