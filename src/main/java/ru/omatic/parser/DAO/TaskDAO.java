package ru.omatic.parser.DAO;

import ru.omatic.parser.dbase.Task;

import java.sql.SQLException;
import java.util.List;
import java.util.Queue;

public interface TaskDAO {
    public void addTask(Task task) throws SQLException;

    public void updateTask(Task task) throws SQLException;

    public Task getTaskById(Long id) throws SQLException;

    public List getAllTasks() throws SQLException;

    public List getAllActiveTasks() throws SQLException;

    public void deleteTask(Task task) throws SQLException;

    public Queue getAllActiveTaskInActiveCategory() throws SQLException;
}