package ru.omatic.parser.DAO;

import ru.omatic.parser.dbase.TaskCategory;
import ru.omatic.parser.dbase.TaskCategory;

import java.sql.SQLException;
import java.util.List;

public interface CategoryDAO {
    public void addCategory(TaskCategory category) throws SQLException;

    public void updateCategory(TaskCategory category) throws SQLException;

    public TaskCategory getCategoryById(Long id) throws SQLException;

    public List getAllCategories() throws SQLException;

    public List getAllActiveCategories() throws SQLException;

    public void deleteCategory(TaskCategory category) throws SQLException;
}