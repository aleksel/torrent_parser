package ru.omatic.parser.url;


import java.io.BufferedReader;
import java.io.IOException;
import java.util.List;

public interface ReceiveDataInterface {
    public BufferedReader getReader() throws IOException;

    public List getHtmlAsList() throws IOException;

    public String getHtmlAsString() throws IOException;
}
