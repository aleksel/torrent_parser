package ru.omatic.parser.url;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/*URL url = new URL("http://stackoverflow.com");

try (BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream(), "UTF-8"))) {
    for (String line; (line = reader.readLine()) != null;) {
        System.out.println(line);
    }
}*/
public class ReceiveData implements ReceiveDataInterface {
    private final Logger logger = (Logger) LogManager.getLogger(ReceiveData.class);
    private BufferedReader reader;

    public ReceiveData(String urlString) {
        try {
            URL url = new URL(urlString);
            this.reader = new BufferedReader(new InputStreamReader(url.openStream(), "windows-1251"));
        } catch (IOException e) {
            logger.error("Ошибка I/O: " + e.getMessage() + ", cause: " + e.getCause().getMessage());
        }
    }

    @Override
    public BufferedReader getReader() {
        return this.reader;
    }

    @Override
    public List getHtmlAsList() {
        List<String> html = new ArrayList<String>();
        if (this.reader != null) {
            try {
                for (String line; (line = reader.readLine()) != null;) {
                    html.add(line);
                }
            } catch (IOException e) {
                logger.error("Ошибка I/O: " + e.getMessage() + ", cause: " + e.getCause().getMessage());
            } finally {
            }
        }
        return html;
    }

    @Override
    public String getHtmlAsString() {
        String html = "";
        if (this.reader != null) {
            try {
                for (String line; (line = reader.readLine()) != null; ) {
                    html += line;
                }
            } catch (IOException e) {
                logger.error("Ошибка I/O: " + e.getMessage() + ", cause: " + e.getCause().getMessage());
            } finally {
            }
        }
        return html;
    }
}
