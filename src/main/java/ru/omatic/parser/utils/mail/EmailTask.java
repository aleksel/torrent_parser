package ru.omatic.parser.utils.mail;

import ru.omatic.parser.dbase.NotificationEmail;
import ru.omatic.parser.dbase.Task;

import java.util.HashMap;
import java.util.Properties;

public class EmailTask implements EmailInterface {

    private Task task = null;
    private String gettingTitle = "";
    private String title = "";
    private HashMap<String, String> settings = null;
    private String emailLayout = "";
    private String emailSubject = "";
    private String emailReceiver = "";

    public EmailTask(Task task, String gettingTitle, String title, HashMap<String, String> settings) {
        this.task = task;
        this.settings = settings;
        this.gettingTitle = gettingTitle;
        this.title = title;

        createEmailReceiver();
        createEmailSubject();
        createEmailLayout();
    }

    /**
     * Getting all emails bounded to the task for send a notification
     * @return
     */
    public void createEmailReceiver() {
        String sendTo = "";
        Object[] taskHasNotification = task.getNotificationEmails().toArray();
        for (Object notificationEmail : taskHasNotification) {
            sendTo += ((NotificationEmail) notificationEmail).getNotificationEmailEmail();
            sendTo += " ";
        }
        emailReceiver = sendTo.isEmpty() ? "" : sendTo.substring(0, (sendTo.length() - 1));
    }

    /**
     * Create a layout for mail notification
     * @return
     */
    public void createEmailLayout() {
        String mailLayout = settings.get("mail.smtp.layout");
        mailLayout = mailLayout.replace("{title}", title);
        mailLayout = mailLayout.replace("{emailSubject}", emailSubject);
        mailLayout = mailLayout.replace("{currentTitle}", gettingTitle);
        mailLayout = mailLayout.replace("{previousTitle}", title);
        mailLayout = mailLayout.replace("{link}", task.getTaskUrl());
        emailLayout = mailLayout;
    }

    /**
     * Get a receive people list
     * @return
     */
    public void createEmailSubject() {
        emailSubject = "Задача: \"" + task.getTaskName() + "\" изменилась.";
    }

    @Override
    public String getEmailSender() {
        return settings.get("mail.smtp.name");
    }

    @Override
    public String getEmailSenderName() {
        return "A torrent has been changed";
    }

    @Override
    public String getEmailPassword() {
        return settings.get("mail.smtp.password");
    }

    @Override
    public String getEmailReceiver() {
        return emailReceiver;
    }

    @Override
    public String getEmailLayout() {
        return emailLayout;
    }

    @Override
    public String getEmailSubject() {
        return emailSubject;
    }

    @Override
    public Properties getEmailProperties() {
        Properties propertis = new Properties();
        propertis.put("mail.smtp.host", settings.get("mail.smtp.host")); //SMTP Host
        propertis.put("mail.smtp.port", settings.get("mail.smtp.port")); //TLS Port
        propertis.put("mail.smtp.auth", "true"); //enable authentication
        propertis.put("mail.smtp.starttls.enable", "true"); //enable STARTTLS
        return propertis;
    }
}
