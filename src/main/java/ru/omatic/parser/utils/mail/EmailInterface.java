package ru.omatic.parser.utils.mail;

import java.util.Properties;

public interface EmailInterface {

    public String getEmailSender();
    public String getEmailSenderName();
    public String getEmailPassword();
    public String getEmailReceiver();
    public String getEmailLayout();
    public String getEmailSubject();
    public Properties getEmailProperties();
}
