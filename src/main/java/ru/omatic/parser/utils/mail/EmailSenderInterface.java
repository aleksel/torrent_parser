package ru.omatic.parser.utils.mail;

public interface EmailSenderInterface {
    public Boolean send(EmailInterface Email);
}
