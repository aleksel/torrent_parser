package ru.omatic.parser.utils.mail;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.Logger;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Date;

public class EmailSender implements EmailSenderInterface {
    private final Logger logger = (Logger) LogManager.getLogger(EmailSender.class);
    @Override
    public Boolean send(final EmailInterface Email) {
        //create Authenticator object to pass in Session.getInstance argument
        Authenticator auth = new Authenticator() {
            //override the getPasswordAuthentication method
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(Email.getEmailSender(), Email.getEmailPassword());
            }
        };
        Session session = Session.getInstance(Email.getEmailProperties(), auth);
        try
        {
            //System.out.println("Message is initializing");
            MimeMessage msg = new MimeMessage(session);
            //set message headers
            msg.addHeader("Content-type", "text/HTML; charset=UTF-8");
            msg.addHeader("format", "flowed");
            msg.addHeader("Content-Transfer-Encoding", "8bit");

            // set field from
            msg.setFrom(new InternetAddress(Email.getEmailSender(), Email.getEmailSenderName()));

            msg.setSubject(Email.getEmailSubject(), "UTF-8");
            msg.setContent(Email.getEmailLayout(), "text/html; charset=UTF-8");
            msg.setSentDate(new Date());
            msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(Email.getEmailReceiver(), false));
            Transport.send(msg);

            return true;
        }
        catch (Exception e) {
            logger.error("Ошибка I/O: " + e.getMessage() + ", cause: " + e.getCause().getMessage());
        }

        return false;
    }
}