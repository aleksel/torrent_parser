package ru.omatic.parser;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.Logger;
import ru.omatic.parser.DAO.Factory;
import ru.omatic.parser.dbase.ParseResult;
import ru.omatic.parser.dbase.Task;
import ru.omatic.parser.url.ReceiveData;
import ru.omatic.parser.utils.mail.EmailSender;
import ru.omatic.parser.utils.mail.EmailTask;

import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Parser extends Thread implements ParserInterface {
    private static final Logger logger = (Logger) LogManager.getLogger(Parser.class);

    private ConcurrentLinkedQueue<Task> queue = null;
    private Task task = null;

    public void setQueue(Queue queue) {
        this.queue = (ConcurrentLinkedQueue<Task>) queue;
    }

    @Override
    public void run() {
        if (queue == null) {
            this.interrupt();
        }

        while (!queue.isEmpty()) {
            try {
                task = queue.remove();
            } catch (NoSuchElementException e) {
                this.interrupt();
            }
            //System.out.println("Thread name: " + Thread.currentThread().getName());
            doWork();
        }
    }

    @Override
    public void doWork() {

        HashMap settings = Factory.getInstance().getSettingDAO().getAllSettingsAsMap();
        // Getting torrent page by task.task_url
        String html = new ReceiveData(task.getTaskUrl()).getHtmlAsString();

        List lastResults = Factory.getInstance().getParseResultDAO().getLastResultByTaskId(task.getTaskId());

        String title = task.getLastTitle(lastResults);

        Matcher m = getMatcher(html, task.getTaskSearchTag());

        String searchTag = getSearchTag(task, title);

        ParseResult result = new ParseResult();
        result.setParseResultRawPage(""/*html*/);
        result.setParseResultTaskId(task.getTaskId());

        if (!html.contains(searchTag) && m.find() && m.group(1) != null) {

            EmailTask email = new EmailTask(task, m.group(1), title, settings);
            Boolean status = new EmailSender().send(email);

            logSendingEmail(task, status, email);

            result.setParseResultPageChanged(1);
            result.setParseResultSearchedElement(m.group(1));

        } else {
            result.setParseResultPageChanged(0);
            result.setParseResultSearchedElement(title);

        }

        Factory.getInstance().getParseResultDAO().addResult(result);

    }

    /**
     * Logging just send an email
     *
     * @param task   Запись из таблицы task
     * @param status Статус отправки емаил оповещения о изменении задачи
     * @param email  Задача на отправку оповещения
     */
    private void logSendingEmail(Task task, Boolean status, EmailTask email) {
        Map<String, String> message = new LinkedHashMap<String, String>();
        message.put("Торрент ", task.getTaskName());
        message.put("Сообщение ", status.toString());
        if (status) {
            message.put("E-mail ", email.getEmailReceiver());
        }
        logger.info(message);
    }

    /**
     * Get a tag with text for search
     *
     * @param task  Запись из таблицы task
     * @param title Заголовок полученный из базы
     * @return string
     */
    private String getSearchTag(Task task, String title) {
        StringBuilder searchString = new StringBuilder();
        if (task.getTaskSearchTag().equals("")) {
            return title;
        } else {
            searchString
                    .append("<")
                    .append(task.getTaskSearchTag())
                    .append(">")
                    .append(title)
                    .append("</")
                    .append(task.getTaskSearchTag())
                    .append(">");
        }
        return searchString.toString();
    }

    /**
     * Obtaining an element from reached page
     *
     * @param html Html код страницы
     * @param tag  Html tag from db
     * @return Matcher
     */
    private Matcher getMatcher(String html, String tag) {
        StringBuilder string = new StringBuilder();
        if (tag.equals("")) {
            string.append("<title>").append("(.*)").append("</title>");
        } else {
            string.append("<").append(tag).append(">").append("(.*)").append("</").append(tag).append(">");
        }
        Pattern p = Pattern.compile(string.toString());
        Matcher m = p.matcher(html);
        m.matches();
        return m;
    }
}
