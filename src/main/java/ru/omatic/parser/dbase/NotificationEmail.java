package ru.omatic.parser.dbase;

import java.sql.Timestamp;
import java.util.Collection;

public class NotificationEmail {
    private int notificationEmailId;
    private String notificationEmailEmail;
    private Timestamp notificationEmailCreateTime;
    private Timestamp notificationEmailUpdateTime;
    private Collection<TaskHasNotificationEmail> taskHasNotificationEmailsByNotificationEmailId;

    public int getNotificationEmailId() {
        return notificationEmailId;
    }

    public void setNotificationEmailId(int notificationEmailId) {
        this.notificationEmailId = notificationEmailId;
    }

    public String getNotificationEmailEmail() {
        return notificationEmailEmail;
    }

    public void setNotificationEmailEmail(String notificationEmailEmail) {
        this.notificationEmailEmail = notificationEmailEmail;
    }

    public Timestamp getNotificationEmailCreateTime() {
        return notificationEmailCreateTime;
    }

    public void setNotificationEmailCreateTime(Timestamp notificationEmailCreateTime) {
        this.notificationEmailCreateTime = notificationEmailCreateTime;
    }

    public Timestamp getNotificationEmailUpdateTime() {
        return notificationEmailUpdateTime;
    }

    public void setNotificationEmailUpdateTime(Timestamp notificationEmailUpdateTime) {
        this.notificationEmailUpdateTime = notificationEmailUpdateTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        NotificationEmail that = (NotificationEmail) o;

        if (notificationEmailId != that.notificationEmailId) return false;
        if (notificationEmailCreateTime != null ? !notificationEmailCreateTime.equals(that.notificationEmailCreateTime) : that.notificationEmailCreateTime != null)
            return false;
        if (notificationEmailEmail != null ? !notificationEmailEmail.equals(that.notificationEmailEmail) : that.notificationEmailEmail != null)
            return false;
        if (notificationEmailUpdateTime != null ? !notificationEmailUpdateTime.equals(that.notificationEmailUpdateTime) : that.notificationEmailUpdateTime != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = notificationEmailId;
        result = 31 * result + (notificationEmailEmail != null ? notificationEmailEmail.hashCode() : 0);
        result = 31 * result + (notificationEmailCreateTime != null ? notificationEmailCreateTime.hashCode() : 0);
        result = 31 * result + (notificationEmailUpdateTime != null ? notificationEmailUpdateTime.hashCode() : 0);
        return result;
    }

    public Collection<TaskHasNotificationEmail> getTaskHasNotificationEmailsByNotificationEmailId() {
        return taskHasNotificationEmailsByNotificationEmailId;
    }

    public void setTaskHasNotificationEmailsByNotificationEmailId(Collection<TaskHasNotificationEmail> taskHasNotificationEmailsByNotificationEmailId) {
        this.taskHasNotificationEmailsByNotificationEmailId = taskHasNotificationEmailsByNotificationEmailId;
    }
}
