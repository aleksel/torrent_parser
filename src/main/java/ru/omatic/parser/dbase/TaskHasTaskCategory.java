package ru.omatic.parser.dbase;

public class TaskHasTaskCategory {
    private int taskHasTaskCategoryId;
    private Task task;
    private TaskCategory taskCategory;

    public int getTaskHasTaskCategoryId() {
        return taskHasTaskCategoryId;
    }

    public void setTaskHasTaskCategoryId(int taskHasTaskCategoryId) {
        this.taskHasTaskCategoryId = taskHasTaskCategoryId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TaskHasTaskCategory that = (TaskHasTaskCategory) o;

        if (taskHasTaskCategoryId != that.taskHasTaskCategoryId) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = taskHasTaskCategoryId;
        return result;
    }

    public Task getTaskByTaskHasTaskCategoryTaskId() {
        return task;
    }

    public void setTaskByTaskHasTaskCategoryTaskId(Task taskByTaskHasTaskCategoryTaskId) {
        this.task = taskByTaskHasTaskCategoryTaskId;
    }

    public TaskCategory getTaskCategoryByTaskHasTaskCategoryTaskCategoryId() {
        return taskCategory;
    }

    public void setTaskCategoryByTaskHasTaskCategoryTaskCategoryId(TaskCategory taskCategoryByTaskHasTaskCategoryTaskCategoryId) {
        this.taskCategory = taskCategoryByTaskHasTaskCategoryTaskCategoryId;
    }
}
