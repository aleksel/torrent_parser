package ru.omatic.parser.dbase;

import java.sql.Timestamp;

public class Setting {
    private int settingId;
    private String settingName;
    private String settingSystemName;
    private String settingValue;
    private String settingType;
    private Timestamp settingCreateTime;
    private Timestamp settingUpdateTime;

    public int getSettingId() {
        return settingId;
    }

    public void setSettingId(int settingId) {
        this.settingId = settingId;
    }

    public String getSettingName() {
        return settingName;
    }

    public void setSettingName(String settingName) {
        this.settingName = settingName;
    }

    public String getSettingSystemName() {
        return settingSystemName;
    }

    public void setSettingSystemName(String settingSystemName) {
        this.settingSystemName = settingSystemName;
    }

    public String getSettingValue() {
        return settingValue;
    }

    public void setSettingValue(String settingValue) {
        this.settingValue = settingValue;
    }

    public String getSettingType() {
        return settingType;
    }

    public void setSettingType(String settingType) {
        this.settingType = settingType;
    }

    public Timestamp getSettingCreateTime() {
        return settingCreateTime;
    }

    public void setSettingCreateTime(Timestamp settingCreateTime) {
        this.settingCreateTime = settingCreateTime;
    }

    public Timestamp getSettingUpdateTime() {
        return settingUpdateTime;
    }

    public void setSettingUpdateTime(Timestamp settingUpdateTime) {
        this.settingUpdateTime = settingUpdateTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Setting that = (Setting) o;

        if (settingId != that.settingId) return false;
        if (settingCreateTime != null ? !settingCreateTime.equals(that.settingCreateTime) : that.settingCreateTime != null)
            return false;
        if (settingName != null ? !settingName.equals(that.settingName) : that.settingName != null) return false;
        if (settingSystemName != null ? !settingSystemName.equals(that.settingSystemName) : that.settingSystemName != null)
            return false;
        if (settingType != null ? !settingType.equals(that.settingType) : that.settingType != null) return false;
        if (settingUpdateTime != null ? !settingUpdateTime.equals(that.settingUpdateTime) : that.settingUpdateTime != null)
            return false;
        if (settingValue != null ? !settingValue.equals(that.settingValue) : that.settingValue != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = settingId;
        result = 31 * result + (settingName != null ? settingName.hashCode() : 0);
        result = 31 * result + (settingSystemName != null ? settingSystemName.hashCode() : 0);
        result = 31 * result + (settingValue != null ? settingValue.hashCode() : 0);
        result = 31 * result + (settingType != null ? settingType.hashCode() : 0);
        result = 31 * result + (settingCreateTime != null ? settingCreateTime.hashCode() : 0);
        result = 31 * result + (settingUpdateTime != null ? settingUpdateTime.hashCode() : 0);
        return result;
    }
}
