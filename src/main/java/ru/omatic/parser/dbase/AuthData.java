package ru.omatic.parser.dbase;

import java.sql.Timestamp;

public class AuthData {
    private int authDataId;
    private String authDataName;
    private String authDataUrl;
    private String authDataData;
    private Timestamp authDataCreateTime;
    private Timestamp authDataUpdateTime;
    private TaskEntity taskByAuthDataTaskId;

    public int getAuthDataId() {
        return authDataId;
    }

    public void setAuthDataId(int authDataId) {
        this.authDataId = authDataId;
    }

    public String getAuthDataName() {
        return authDataName;
    }

    public void setAuthDataName(String authDataName) {
        this.authDataName = authDataName;
    }

    public String getAuthDataUrl() {
        return authDataUrl;
    }

    public void setAuthDataUrl(String authDataUrl) {
        this.authDataUrl = authDataUrl;
    }

    public String getAuthDataData() {
        return authDataData;
    }

    public void setAuthDataData(String authDataData) {
        this.authDataData = authDataData;
    }

    public Timestamp getAuthDataCreateTime() {
        return authDataCreateTime;
    }

    public void setAuthDataCreateTime(Timestamp authDataCreateTime) {
        this.authDataCreateTime = authDataCreateTime;
    }

    public Timestamp getAuthDataUpdateTime() {
        return authDataUpdateTime;
    }

    public void setAuthDataUpdateTime(Timestamp authDataUpdateTime) {
        this.authDataUpdateTime = authDataUpdateTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        AuthData that = (AuthData) o;

        if (authDataId != that.authDataId) return false;
        if (authDataCreateTime != null ? !authDataCreateTime.equals(that.authDataCreateTime) : that.authDataCreateTime != null)
            return false;
        if (authDataData != null ? !authDataData.equals(that.authDataData) : that.authDataData != null) return false;
        if (authDataName != null ? !authDataName.equals(that.authDataName) : that.authDataName != null) return false;
        if (authDataUpdateTime != null ? !authDataUpdateTime.equals(that.authDataUpdateTime) : that.authDataUpdateTime != null)
            return false;
        if (authDataUrl != null ? !authDataUrl.equals(that.authDataUrl) : that.authDataUrl != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = authDataId;
        result = 31 * result + (authDataName != null ? authDataName.hashCode() : 0);
        result = 31 * result + (authDataUrl != null ? authDataUrl.hashCode() : 0);
        result = 31 * result + (authDataData != null ? authDataData.hashCode() : 0);
        result = 31 * result + (authDataCreateTime != null ? authDataCreateTime.hashCode() : 0);
        result = 31 * result + (authDataUpdateTime != null ? authDataUpdateTime.hashCode() : 0);
        return result;
    }
}
