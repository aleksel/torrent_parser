package ru.omatic.parser.dbase;

import java.sql.Timestamp;
import java.util.Collection;
import java.util.List;
import java.util.Set;

public class Task {
    private int taskId;
    private String taskUrl;
    private String taskName;
    private String taskComparisonElement;
    private int taskActive;
    private Timestamp taskCreateTime;
    private Timestamp taskUpdateTime;
    private Collection<ParseResult> parseResultsByTaskId;
    private Collection<TaskHasNotificationEmail> taskHasNotificationEmailsByTaskId;
    private Collection<TaskHasTaskCategory> taskHasTaskCategoriesByTaskId;
    private Integer taskAuthDataId;
    private AuthData authData;
    private Set<NotificationEmail> notificationEmails;
    private String taskSearchTag;
    private String title;

    public String getTaskSearchTag() {
        return taskSearchTag;
    }

    public void setTaskSearchTag(String taskSearchTag) {
        this.taskSearchTag = taskSearchTag;
    }

    public Set<NotificationEmail> getNotificationEmails() {
        return notificationEmails;
    }

    public void setNotificationEmails(Set<NotificationEmail> notificationEmails) {
        this.notificationEmails = notificationEmails;
    }

    public AuthData getAuthData() {
        return authData;
    }

    public void setAuthData(AuthData authData) {
        this.authData = authData;
    }

    public Integer getTaskAuthDataId() {
        return taskAuthDataId;
    }

    public void setTaskAuthDataId(Integer taskAuthDataId) {
        this.taskAuthDataId = taskAuthDataId;
    }

    public int getTaskId() {
        return taskId;
    }

    public void setTaskId(int taskId) {
        this.taskId = taskId;
    }

    public String getTaskUrl() {
        return taskUrl;
    }

    public void setTaskUrl(String taskUrl) {
        this.taskUrl = taskUrl;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public String getTaskComparisonElement() {
        return taskComparisonElement;
    }

    public void setTaskComparisonElement(String taskComparisonElement) {
        this.taskComparisonElement = taskComparisonElement;
    }

    public int getTaskActive() {
        return taskActive;
    }

    public void setTaskActive(int taskActive) {
        this.taskActive = taskActive;
    }

    public Timestamp getTaskCreateTime() {
        return taskCreateTime;
    }

    public void setTaskCreateTime(Timestamp taskCreateTime) {
        this.taskCreateTime = taskCreateTime;
    }

    public Timestamp getTaskUpdateTime() {
        return taskUpdateTime;
    }

    public void setTaskUpdateTime(Timestamp taskUpdateTime) {
        this.taskUpdateTime = taskUpdateTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Task that = (Task) o;

        if (taskActive != that.taskActive) return false;
        if (taskId != that.taskId) return false;
        if (taskComparisonElement != null ? !taskComparisonElement.equals(that.taskComparisonElement) : that.taskComparisonElement != null)
            return false;
        if (taskCreateTime != null ? !taskCreateTime.equals(that.taskCreateTime) : that.taskCreateTime != null)
            return false;
        if (taskName != null ? !taskName.equals(that.taskName) : that.taskName != null) return false;
        if (taskUpdateTime != null ? !taskUpdateTime.equals(that.taskUpdateTime) : that.taskUpdateTime != null)
            return false;
        if (taskUrl != null ? !taskUrl.equals(that.taskUrl) : that.taskUrl != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = taskId;
        result = 31 * result + (taskUrl != null ? taskUrl.hashCode() : 0);
        result = 31 * result + (taskName != null ? taskName.hashCode() : 0);
        result = 31 * result + (taskComparisonElement != null ? taskComparisonElement.hashCode() : 0);
        result = 31 * result + taskActive;
        result = 31 * result + (taskCreateTime != null ? taskCreateTime.hashCode() : 0);
        result = 31 * result + (taskUpdateTime != null ? taskUpdateTime.hashCode() : 0);
        return result;
    }

    public Collection<ParseResult> getParseResultsByTaskId() {
        return parseResultsByTaskId;
    }

    public void setParseResultsByTaskId(Collection<ParseResult> parseResultsByTaskId) {
        this.parseResultsByTaskId = parseResultsByTaskId;
    }

    public Collection<TaskHasNotificationEmail> getTaskHasNotificationEmailsByTaskId() {
        return taskHasNotificationEmailsByTaskId;
    }

    public void setTaskHasNotificationEmailsByTaskId(Collection<TaskHasNotificationEmail> taskHasNotificationEmailsByTaskId) {
        this.taskHasNotificationEmailsByTaskId = taskHasNotificationEmailsByTaskId;
    }

    public Collection<TaskHasTaskCategory> getTaskHasTaskCategoriesByTaskId() {
        return taskHasTaskCategoriesByTaskId;
    }

    public void setTaskHasTaskCategoriesByTaskId(Collection<TaskHasTaskCategory> taskHasTaskCategoriesByTaskId) {
        this.taskHasTaskCategoriesByTaskId = taskHasTaskCategoriesByTaskId;
    }

    /**
     * Create title for searching either from last parsing element or task.task_comparison_element
     *
     * @param lastResults
     * @return
     */
    public String getLastTitle(List<ParseResult> lastResults) {
        if (this.title != null) {
            return this.title;
        }
        String title;
        if (!lastResults.isEmpty()) {
            ParseResult lastResult = lastResults.get(0);
            title = lastResult.getParseResultSearchedElement();
        } else {
            title = getTaskComparisonElement();
        }
        this.title = title;
        return title;
    }
}
