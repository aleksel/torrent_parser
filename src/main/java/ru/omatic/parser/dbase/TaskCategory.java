package ru.omatic.parser.dbase;

import java.sql.Timestamp;
import java.util.Set;

public class TaskCategory {
    private int taskCategoryId;
    private String taskCategoryName;
    private int taskCategoryActive;
    private Timestamp taskCategoryCreateTime;
    private Timestamp taskCategoryUpdateTime;
    private Set<TaskHasTaskCategory> tasks;

    public Set<TaskHasTaskCategory> getTasks() {
        return tasks;
    }

    public void setTasks(Set<TaskHasTaskCategory> tasks) {
        this.tasks = tasks;
    }

    public int getTaskCategoryId() {
        return taskCategoryId;
    }

    public void setTaskCategoryId(int taskCategoryId) {
        this.taskCategoryId = taskCategoryId;
    }

    public String getTaskCategoryName() {
        return taskCategoryName;
    }

    public void setTaskCategoryName(String taskCategoryName) {
        this.taskCategoryName = taskCategoryName;
    }

    public int getTaskCategoryActive() {
        return taskCategoryActive;
    }

    public void setTaskCategoryActive(int taskCategoryActive) {
        this.taskCategoryActive = taskCategoryActive;
    }

    public Timestamp getTaskCategoryCreateTime() {
        return taskCategoryCreateTime;
    }

    public void setTaskCategoryCreateTime(Timestamp taskCategoryCreateTime) {
        this.taskCategoryCreateTime = taskCategoryCreateTime;
    }

    public Timestamp getTaskCategoryUpdateTime() {
        return taskCategoryUpdateTime;
    }

    public void setTaskCategoryUpdateTime(Timestamp taskCategoryUpdateTime) {
        this.taskCategoryUpdateTime = taskCategoryUpdateTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TaskCategory that = (TaskCategory) o;

        if (taskCategoryActive != that.taskCategoryActive) return false;
        if (taskCategoryId != that.taskCategoryId) return false;
        if (taskCategoryCreateTime != null ? !taskCategoryCreateTime.equals(that.taskCategoryCreateTime) : that.taskCategoryCreateTime != null)
            return false;
        if (taskCategoryName != null ? !taskCategoryName.equals(that.taskCategoryName) : that.taskCategoryName != null)
            return false;
        if (taskCategoryUpdateTime != null ? !taskCategoryUpdateTime.equals(that.taskCategoryUpdateTime) : that.taskCategoryUpdateTime != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = taskCategoryId;
        result = 31 * result + (taskCategoryName != null ? taskCategoryName.hashCode() : 0);
        result = 31 * result + taskCategoryActive;
        result = 31 * result + (taskCategoryCreateTime != null ? taskCategoryCreateTime.hashCode() : 0);
        result = 31 * result + (taskCategoryUpdateTime != null ? taskCategoryUpdateTime.hashCode() : 0);
        return result;
    }
}
