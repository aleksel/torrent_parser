package ru.omatic.parser.dbase;

import java.sql.Timestamp;

public class ParseResult {
    private int parseResultId;
    private int parseResultTaskId;
    private int parseResultPageChanged;
    private String parseResultRawPage;
    private String parseResultSearchedElement;
    private Timestamp parseResultCreateTime;
    private Task taskByParseResultTaskId;

    public int getParseResultId() {
        return parseResultId;
    }

    public void setParseResultId(int parseResultId) {
        this.parseResultId = parseResultId;
    }

    public int getParseResultTaskId() {
        return parseResultTaskId;
    }

    public void setParseResultTaskId(int parseResultTaskId) {
        this.parseResultTaskId = parseResultTaskId;
    }

    public int getParseResultPageChanged() {
        return parseResultPageChanged;
    }

    public void setParseResultPageChanged(int parseResultPageChanged) {
        this.parseResultPageChanged = parseResultPageChanged;
    }

    public String getParseResultRawPage() {
        return parseResultRawPage;
    }

    public void setParseResultRawPage(String parseResultRawPage) {
        this.parseResultRawPage = parseResultRawPage;
    }

    public String getParseResultSearchedElement() {
        return parseResultSearchedElement;
    }

    public void setParseResultSearchedElement(String parseResultSearchedElement) {
        this.parseResultSearchedElement = parseResultSearchedElement;
    }

    public Timestamp getParseResultCreateTime() {
        return parseResultCreateTime;
    }

    public void setParseResultCreateTime(Timestamp parseResultCreateTime) {
        this.parseResultCreateTime = parseResultCreateTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ParseResult that = (ParseResult) o;

        if (parseResultId != that.parseResultId) return false;
        if (parseResultPageChanged != that.parseResultPageChanged) return false;
        if (parseResultTaskId != that.parseResultTaskId) return false;
        if (parseResultSearchedElement != null ? !parseResultSearchedElement.equals(that.parseResultSearchedElement) : that.parseResultSearchedElement != null)
            return false;
        if (parseResultCreateTime != null ? !parseResultCreateTime.equals(that.parseResultCreateTime) : that.parseResultCreateTime != null)
            return false;
        if (parseResultRawPage != null ? !parseResultRawPage.equals(that.parseResultRawPage) : that.parseResultRawPage != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = parseResultId;
        result = 31 * result + parseResultTaskId;
        result = 31 * result + parseResultPageChanged;
        result = 31 * result + (parseResultRawPage != null ? parseResultRawPage.hashCode() : 0);
        result = 31 * result + (parseResultSearchedElement != null ? parseResultSearchedElement.hashCode() : 0);
        result = 31 * result + (parseResultCreateTime != null ? parseResultCreateTime.hashCode() : 0);
        return result;
    }

    public Task getTaskByParseResultTaskId() {
        return taskByParseResultTaskId;
    }

    public void setTaskByParseResultTaskId(Task taskByParseResultTaskId) {
        this.taskByParseResultTaskId = taskByParseResultTaskId;
    }
}
