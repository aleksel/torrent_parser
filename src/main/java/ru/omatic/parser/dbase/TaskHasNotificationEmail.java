package ru.omatic.parser.dbase;

public class TaskHasNotificationEmail {
    private int taskHasNotificationEmailId;
    private int taskHasNotificationEmailTaskId;
    private int taskHasNotificationEmailNotificationEmailId;
    private Task taskByTaskHasNotificationEmailTaskId;
    private NotificationEmail notificationEmailByTaskHasNotificationEmailNotificationEmailId;

    public int getTaskHasNotificationEmailId() {
        return taskHasNotificationEmailId;
    }

    public void setTaskHasNotificationEmailId(int taskHasNotificationEmailId) {
        this.taskHasNotificationEmailId = taskHasNotificationEmailId;
    }

    public int getTaskHasNotificationEmailTaskId() {
        return taskHasNotificationEmailTaskId;
    }

    public void setTaskHasNotificationEmailTaskId(int taskHasNotificationEmailTaskId) {
        this.taskHasNotificationEmailTaskId = taskHasNotificationEmailTaskId;
    }

    public int getTaskHasNotificationEmailNotificationEmailId() {
        return taskHasNotificationEmailNotificationEmailId;
    }

    public void setTaskHasNotificationEmailNotificationEmailId(int taskHasNotificationEmailNotificationEmailId) {
        this.taskHasNotificationEmailNotificationEmailId = taskHasNotificationEmailNotificationEmailId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TaskHasNotificationEmail that = (TaskHasNotificationEmail) o;

        if (taskHasNotificationEmailId != that.taskHasNotificationEmailId) return false;
        if (taskHasNotificationEmailNotificationEmailId != that.taskHasNotificationEmailNotificationEmailId)
            return false;
        if (taskHasNotificationEmailTaskId != that.taskHasNotificationEmailTaskId) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = taskHasNotificationEmailId;
        result = 31 * result + taskHasNotificationEmailTaskId;
        result = 31 * result + taskHasNotificationEmailNotificationEmailId;
        return result;
    }

    public Task getTaskByTaskHasNotificationEmailTaskId() {
        return taskByTaskHasNotificationEmailTaskId;
    }

    public void setTaskByTaskHasNotificationEmailTaskId(Task taskByTaskHasNotificationEmailTaskId) {
        this.taskByTaskHasNotificationEmailTaskId = taskByTaskHasNotificationEmailTaskId;
    }

    public NotificationEmail getNotificationEmailByTaskHasNotificationEmailNotificationEmailId() {
        return notificationEmailByTaskHasNotificationEmailNotificationEmailId;
    }

    public void setNotificationEmailByTaskHasNotificationEmailNotificationEmailId(NotificationEmail notificationEmailByTaskHasNotificationEmailNotificationEmailId) {
        this.notificationEmailByTaskHasNotificationEmailNotificationEmailId = notificationEmailByTaskHasNotificationEmailNotificationEmailId;
    }
}
