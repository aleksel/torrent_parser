package ru.omatic.parser;

import ru.omatic.parser.DAO.Factory;
import ru.omatic.parser.dbase.Task;
import ru.omatic.parser.utils.HibernateUtil;

import java.util.concurrent.ConcurrentLinkedQueue;

public class Main {
    public static void main(final String[] args) {

        ConcurrentLinkedQueue<Task> queue = (ConcurrentLinkedQueue<Task>) Factory.getInstance().getTaskDAO().getAllActiveTaskInActiveCategory();
        Parser[] parsers = new Parser[10];

        for (int i = 0; i < parsers.length; ++i) {
            parsers[i] = new Parser();
            parsers[i].setQueue(queue);
            parsers[i].start();
        }

        for (int i = 0; i < parsers.length; ++i) {
            try {
                parsers[i].join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        HibernateUtil.getSessionFactory().close();
    }
}
