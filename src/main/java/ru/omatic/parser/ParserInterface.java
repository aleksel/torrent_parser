package ru.omatic.parser;

import java.util.Queue;

public interface ParserInterface {
    public void run();
    public void doWork();
    public void setQueue(Queue queue);
}
