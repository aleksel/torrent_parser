#!/bin/sh

progname=torrent_parser
logfile=$progname.log
errorfile=$progname.err

java -Dlog4j.configurationFile=src\main\resources\config\log4j\log4j2.xml -classpath "classes;lib/log4j-api-2.1.jar;lib/log4j-core-2.1.jar;lib/hibernate-core-4.3.7.Final.jar;lib/jboss-logging-3.1.3.GA.jar;lib/jboss-logging-annotations-1.2.0.Beta1.jar;lib/jboss-transaction-api_1.2_spec-1.0.0.Final.jar;lib/dom4j-1.6.1.jar;lib/xml-apis-1.0.b2.jar;lib/hibernate-commons-annotations-4.0.5.Final.jar;lib/hibernate-jpa-2.1-api-1.0.0.Final.jar;lib/javassist-3.18.1-GA.jar;lib/antlr-2.7.7.jar;lib/jandex-1.1.0.Final.jar;lib/javax.mail-1.5.2.jar;lib/activation-1.1.jar;lib/mysql-connector-java-5.1.34.jar" ru.omatic.parser.Main > $logfile 2> $errorfile

